<?php

session_start();

require 'headers.php';

if (!empty($_POST['parcours'])) {
	$parcours = $_POST['parcours'];
	if (file_exists('../fichiers/' . $parcours)) {
		$fichiers = glob('../fichiers/' . $parcours . '/' . '*.*');
		foreach ($fichiers as $f) {
			unlink($f);
		}
	}
	echo 'dossier_vide';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
