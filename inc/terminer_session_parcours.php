<?php

session_start();

require 'headers.php';

if (!empty($_POST['parcours'])) {
	$parcours = $_POST['parcours'];
	unset($_SESSION['digisteps'][$parcours]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
